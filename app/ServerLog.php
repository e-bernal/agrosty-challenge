<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServerLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'status', 'date', 'server_id'
    ];

    public function server()
    {
        return $this->belongsTo('App\Server');
    }
}
