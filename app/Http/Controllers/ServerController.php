<?php

namespace App\Http\Controllers;

use App\Server;
use App\ServerLog;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $servers = Server::get();
      // return view('servers/index')->with([
      //   'servers' => $servers
      // ]);
      return view('servers/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('servers/form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try{
        $server = Server::create($request->all());
        $message = 'Servidor creado exitosamente';
        $status = 'success';
      }
      catch(\Exception $e)
      {
        $message = 'Error al crear servidor, comuníquese con el administrador';
        $status = 'danger';
      }

      $servers = Server::get();
      return view('servers/index')->with([
        'servers' => $servers,
        'status' => $status,
        'message' => $message
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function show(Server $server)
    {
        $logs = ServerLog::where('server_id', $server->id)->paginate(7);
        return view('servers/logs')->with([
            'server' => $server,
            'logs' => $logs
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function edit(Server $server)
    {
        return view('servers/form')->with([
          'method' => 'PATCH',
          'server' => $server
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Server $server)
    {
      dd('hola');
      try{
        $server::update($request->all());
        $message = 'Servidor editado exitosamente';
        $status = 'success';
      }
      catch(\Exception $e)
      {
        $message = 'Error al editar servidor, comuníquese con el administrador';
        $status = 'danger';
      }

      $servers = Server::get();
      return view('servers/index')->with([
        'servers' => $servers,
        'status' => $status,
        'message' => $message
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function destroy(Server $server)
    {
      try{
        $server->delete();
        $message = 'Servidor eliminado exitosamente';
        $status = 'success';
      }
      catch(\Exception $e)
      {
        $message = 'Error al eliminar servidor, comuníquese con el administrador';
        $status = 'danger';
      }

      $servers = Server::get();
      return view('servers/index')->with([
        'servers' => $servers,
        'status' => $status,
        'message' => $message
      ]);
    }
}
