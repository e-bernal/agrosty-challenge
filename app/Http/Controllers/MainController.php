<?php

namespace App\Http\Controllers;

use App\Server;
use App\ServerLog;
use App\Charts\Servers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function color_rand() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
  }

  public function index()
  {
      if (Auth::id())
      {
        $servers = Server::all();
        $labels = ['Not Found', 'Conflict', 'Internal Server Error'];
        $chart = new Servers;
        $chart->labels($labels);

        foreach($servers as $server)
        {
          $color = $this->color_rand();
          $error404 = $server->logs()->where('status', 'Not Found')->count();
          $error409 = $server->logs()->where('status', 'Conflict')->count();
          $error500 = $server->logs()->where('status', 'Internal Server Error')->count();
          $data = [$error404, $error409, $error500];
          $chart->dataset($server->name, 'line', $data)->options([
            'borderColor' => $color,
          ]);
        }
        //dd($chart);

        return view('dashboard/dashboard')->with(['chart' => $chart]);
      }
      else
      {
          return view('auth/login');
      }
  }

  public function serversTable(){
    $servers = Server::get();
    return view('servers/table')->with([
      'servers' => $servers
    ]);
  }

  public function getServers()
  {
    $servers = Server::get();
    return $servers->toJson();
  }

  public function apiServers(Request $request)
  {
    $res = $this->responseServer();
    if($res == 404) {
      $status = 'Not Found';
    }
    else if($res == 409) {
      $status = 'Conflict';
    }
    else if($res == 500) {
      $status = 'Internal Server Error';
    }
    else {
      $status = 'Active';
    }

    $serverLog = new ServerLog();
    $serverLog->server_id = $request->id;
    $serverLog->status = $status;
    $serverLog->save();

    $server = Server::find($request->id);
    $server->update(['status' => $status]);

    return route('severstable');
  }

  public function responseServer(){
    $responses = [200, 200, 200, 200, 200, 200, 200, 404, 409, 500];
    $res = $responses[mt_rand(0, 9)];
    return $res;
  }
}
