<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'url', 'status'
    ];

    public function logs()
    {
        return $this->hasMany('App\ServerLog', 'server_id', 'id');
    }
}
