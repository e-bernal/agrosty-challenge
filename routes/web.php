<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');
Auth::routes();
// servers functions routes
Route::resource('servers', 'ServerController');
Route::get('/table', 'MainController@serversTable')->name('severstable');
// api routes
Route::get('/getServers', 'MainController@getServers');
Route::get('/checkServer', 'MainController@apiServers');
