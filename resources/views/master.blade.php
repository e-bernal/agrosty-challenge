<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Risorse | Reports</title>
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar has-shadow">
            <div class="container">
                <div class="navbar-brand">
                    <a class ="navbar-item" href="{{ url('/') }}">
                      <img src="{{ asset('images/agrosty.png') }}">
                    </a>
                    <div class="navbar-burger burger" data-target="menu-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="navbar-menu" id="menu-toggle">
                    <div class="navbar-start">
                        <a class="navbar-item is-tab is-hidden-tablet">
                            <span class="icon"><i class="fa fa-home"></i></span> Inicio
                        </a>
                        <a href="/servers" class="navbar-item is-tab is-hidden-tablet">
                            <span class="icon"><i class="fa fa-server"></i></span> Servidores
                        </a>
                    </div>
                    <div class="navbar-end">
                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link" href="#">{{ Auth::user()->name }}</a>
                            <div class="navbar-dropdown">
                                <a class="navbar-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <section class=" columns is-fullheight">
            {{-- side menu --}}
            <aside class="main-menu column is-2 is-narrow-mobile is-fullheight section is-hidden-mobile menu">
                <ul class="menu-list">
                    <li>
                        <a href="/" class="home">
                        <span class="icon"><i class="fa fa-home"></i></span> Inicio
                        </a>
                    </li>
                    <li>
                        <a href="/servers" class="reportes">
                            <span class="icon"><i class="fa fa-server"></i></span> Servidores
                        </a>
                    </li>
                </ul>
            </aside>
            {{-- Work area --}}
            <div class="container column is-10">
                    @yield('content')
            </div>
        </section>
    </div>

  <!-- Scripts -->
  <script src="{{ mix('js/app.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  @yield('scripts')
</body>
</html>
