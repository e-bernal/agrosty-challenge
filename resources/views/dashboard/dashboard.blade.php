@extends('master')
@section('content')
  <section class="hero">
    <div class="hero-body">
      <div class="container">
          <p class="title">Servidores</p>
          <p class="subtitle">Estadística</p>
      </div>
    </div>
  </section>
  <div class="section is-content">
    {!! $chart->container() !!}
  </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
  {!! $chart->script() !!}
@endsection
