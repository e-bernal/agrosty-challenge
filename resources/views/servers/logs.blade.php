@extends('master')
@section('content')
  <section class="hero">
    <div class="hero-body">
      <div class="container">
        <p class="title">Servidores</p>
        <p class="subtitle">{{ $server->name }}</p>
        <nav class="breadcrumb" aria-label="breadcrumbs">
          <ul>
            <li style="display:none"></li>
            <li><a href="/">Inicio</a></li>
            <li><a href="/servers">Servidores</a></li>
            <li class="is-active">{{ $server->name }}</li>
          </ul>
        </nav>
      </div>
    </div>
  </section>
  <div class="section is-content">
    <table class="table is-striped">
      <thead>
        <th>Estatus</th>
        <th>Última revisión</th>
      </thead>
      <tbody>
        @if (!empty($logs))
          @foreach ($logs as $log)
            <tr>
              <td><span class="{{ ($log->status == 'Active') ? 'tag is-success' : 'tag is-danger' }}">{{ $log->status }}</span></td>
              <td>{{ $log->created_at }}</td>
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">No hay registros.</td>
          </tr>
        @endif
      </tbody>
    </table>
    {{ $logs->links() }}
  </div>
@endsection
