@extends('master')
@section('content')
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <p class="title">Servidores</p>
                <p class="subtitle">{{ empty($server)?'Crear nuevo':'Editar' }} servidor</p>
                <nav class="breadcrumb" aria-label="breadcrumbs">
                    <ul>
                        <li></li>
                        <li><a href="/">Inicio</a></li>
                        <li><a href="{{ route('servers.index') }}">Servidores</a></li>
                        <li class="is-active"><a href="#">{{ empty($server)?'Crear nuevo':'Editar' }} servidor</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
    <div class="section">
        <div class="columns">
            <div class="column is-four-fifths">
              <form method="{{ empty($method) ? 'POST' : $method}}" action="{{ empty($method) ? route('servers.store') : route('servers.update', ['server' => $server]) }}">
                    @csrf
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Nombre</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input class="input" type="text" name="name" value="{{ empty($server)?'':$server->name }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">URL: </label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                  <input class="input" type="url" name="url" value="{{ empty($server)?'':$server->url }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label"></div>
                        <div class="field-body">
                            <div class="field is-grouped">
                                <div class="control">
                                    <input type="submit" class="button is-link" value="Aceptar">
                                </div>
                                @if(empty($server))
                                  <div class="control">
                                    <input type="reset" class="button is-text" value="Cancelar">
                                  </div>
                                @else
                                  <div class="control">
                                    <a href="{{ route('servers.index') }}" class="button is-text">Cancelar</a>
                                  </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="column"></div>
        </div>
    </div>
@endsection
