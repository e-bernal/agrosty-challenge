<table class="table is-striped">
    <thead>
        <th>Nombre</th>
        <th>URL</th>
        <th>Estatus</th>
        <th>Última revisión</th>
        <th></th>
        <th></th>
        <th></th>
    </thead>
    <tbody>
        @if (!empty($servers))
            @foreach ($servers as $server)
                <tr>
                    <td>{{ $server->name }}</td>
                    <td>{{ $server->url }}</td>
                    <td><span class="{{ ($server->status == 'Active' || $server->status == 'active') ? 'tag is-success' : 'tag is-danger' }}">{{ $server->status }}</span></td>
                    <td>{{ empty($server->updated_at) ? $server->created_at : $server->updated_at }}</td>
                    <td>
                      <a href="{{ route('servers.show', ['server'=>$server]) }}" class="is-info">
                        <i class="fa fa-eye"></i>
                      </a>
                    </td>
                    <td>
                        <a href="{{ route('servers.edit', ['server'=>$server]) }}" class="is-edit">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                    <td>
                        {!! Form::open(['url' => route('servers.destroy', ['server'=>$server]), 'method' => 'delete']) !!}
                            {{ Form::token() }}
                            {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn is-delete'] )  }}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">No hay servidores registrados.</td>
            </tr>
        @endif
    </tbody>
</table>
