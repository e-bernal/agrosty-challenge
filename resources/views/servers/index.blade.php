@extends('master')
@section('content')
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <p class="title">Servidores</p>
                <p class="subtitle">Lista de Servidores</p>
                <nav class="breadcrumb" aria-label="breadcrumbs">
                        <ul>
                            <li style="display:none"></li>
                            <li><a href="/">Inicio</a></li>
                            <li class="is-active"><a href="servers">Servidores</a></li>
                        </ul>
                    </nav>
            </div>
        </div>
    </section>
    <div class="section is-content">
        <div class="columns">
            <div class="column is-four-fifths"></div>
            <div class="column">
                <a class="button is-link" href="{{ route('servers.create') }}"><i class="fa fa-plus"></i> &nbsp; Nuevo</a>
            </div>
        </div>
        @if (!empty($message))
            <div class="notification is-{{ $status }}">
                <button class="delete"></button>
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="servers-table">
          @include('servers/table')
        </div>
    </div>
@endsection
