<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Agrosty | Challenge</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <section class="hero is-primary">
            <div class="hero-body">
                <div class="container">
                    <h1 class="title">
                        Agrosty Challenge
                    </h1>
                </div>
            </div>
        </section>

        <div class="columns is-marginless is-centered">
            <div class="column is-5">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">Ingresar</p>
                    </header>

                    <div class="card-content">
                        <form class="login-form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="field is-horizontal">
                                <div class="field-label">
                                    <label class="label">Correo electrónico</label>
                                </div>

                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" id="email" type="email" name="email"
                                                value="{{ old('email') }}" required autofocus>
                                        </p>

                                        @if ($errors->has('email'))
                                            <p class="help is-danger">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label">
                                    <label class="label">Contraseña</label>
                                </div>

                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" id="password" type="password" name="password" required>
                                        </p>

                                        @if ($errors->has('password'))
                                            <p class="help is-danger">
                                                {{ $errors->first('password') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>

                                <div class="field-body">
                                    <div class="field is-grouped">
                                        <div class="control">
                                            <button type="submit" class="button is-primary">Login</button>
                                        </div>

                                        <div class="control">
                                            <a href="{{ route('password.request') }}">
                                                Olvidé mi contraseña
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

