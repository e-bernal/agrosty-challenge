$('.main-menu a').click(function() {
    console.log('click');
    var activeItem = $('.main-menu .is-active');
    console.log(activeItem);
    if (activeItem) {
        activeItem.removeClass('is-active');
    }
    this.classList.add('is-active');
});

$(document).ready(function() {
    var url = $(location).attr("pathname");
    console.log((url == '/'))
    if (url == '/') {
        $('.main-menu .home').addClass('is-active');
    }

    var url_2 = $(location).attr("origin") + '/table';
    $('div.servers-table').fadeOut();
    $('div.servers-table').load(url_2, function() {
        $('div.servers-table').fadeIn();
    });

    var servers;

    function getServers() {
        $.ajax({
            type: "GET",
            url: '/getServers',
        }).done(function(result) {
            servers = result
        })
    }
    setInterval(getServers, 25000);

    function callApi() {
        console.log(servers);
        $.each(JSON.parse(servers), function(index, item) {
            $.ajax({
                type: "GET",
                url: '/checkServer',
                data: {
                    "id": item.id,
                    'url': item.url
                }
            }).done(function(url) {
                console.log(url);
                $('div.servers-table').fadeOut();
                $('div.servers-table').load(url, function() {
                    $('div.servers-table').fadeIn();
                });
            })
        });
    }
    setInterval(callApi, 26000);
});